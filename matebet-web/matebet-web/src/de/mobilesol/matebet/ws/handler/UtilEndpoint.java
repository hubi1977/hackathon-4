package de.mobilesol.matebet.ws.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.service.CleanupService;
import de.mobilesol.matebet.web.util.Util;

public class UtilEndpoint {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(UtilEndpoint.class.getName());

	public Object handleClean(HttpServletResponse resp) throws IOException {

		CleanupService s = new CleanupService();
		s.getClosedBets(0);

		Object o = Util.getCache().getCacheStatistics();
		Util.getCache().clear();
		return o;
	}
}
