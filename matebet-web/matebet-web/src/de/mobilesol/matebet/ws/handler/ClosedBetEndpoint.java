package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.service.ClosedBetService;

public class ClosedBetEndpoint {

	private static final long serialVersionUID = 1L;

	private ClosedBetService closedBetService = new ClosedBetService();

	public List<BetDTO> handleClosedBets(int userId, HttpServletResponse resp)
			throws IOException {
		List<BetDTO> u = closedBetService.getClosedBets(userId);
		return u;
	}

	public BetDTO handleClosedBet(int betId, int userId,
			HttpServletResponse resp) throws IOException {
		BetDTO b = closedBetService.getClosedBet(betId, userId);
		return b;
	}

	public Object handleArchived(int betId, int userId,
			HttpServletResponse resp) throws IOException {

		closedBetService.setArchived(betId, userId);
		return null;
	}

	public Object handleDelete(int betId, int userId, HttpServletResponse resp)
			throws IOException {

		closedBetService.delete(betId, userId);
		return null;
	}
}
