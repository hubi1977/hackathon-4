package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.service.ActiveBetService;

public class ActiveBetEndpoint {

	private static final long serialVersionUID = 1L;

	private ActiveBetService activeBetService = new ActiveBetService();

	public List<BetDTO> handleOpenBets(int userId, HttpServletResponse resp)
			throws IOException {
		List<BetDTO> u = activeBetService.getOpenBets(userId);

		return u;
	}

	public BetDTO handleOpenBet(int betId, int userId, HttpServletResponse resp)
			throws IOException {
		BetDTO u = activeBetService.getOpenBet(betId, userId);

		return u;
	}
}
