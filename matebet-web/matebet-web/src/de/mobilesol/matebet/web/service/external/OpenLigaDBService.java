package de.mobilesol.matebet.web.service.external;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.util.ComparatorUtil;
import de.msiggi.sportsdata.webservices.ArrayOfGroup;
import de.msiggi.sportsdata.webservices.ArrayOfLeague;
import de.msiggi.sportsdata.webservices.ArrayOfMatchdata;
import de.msiggi.sportsdata.webservices.ArrayOfSport;
import de.msiggi.sportsdata.webservices.ArrayOfTeam;
import de.msiggi.sportsdata.webservices.Group;
import de.msiggi.sportsdata.webservices.League;
import de.msiggi.sportsdata.webservices.MatchResult;
import de.msiggi.sportsdata.webservices.Matchdata;
import de.msiggi.sportsdata.webservices.Sport;
import de.msiggi.sportsdata.webservices.Sportsdata;
import de.msiggi.sportsdata.webservices.SportsdataSoap;
import de.msiggi.sportsdata.webservices.Team;

public class OpenLigaDBService {

	private static final Logger log = Logger
			.getLogger(OpenLigaDBService.class.getName());

	private SportsdataSoap service;

	private static List<LeagueDTO> leagueCache = null;

	private static Map<Integer, LeagueDTO> leagueMapCache = new HashMap<Integer, LeagueDTO>();

	private static Map<Integer, List<MatchDTO>> matchMapCache = new HashMap<Integer, List<MatchDTO>>();
	private static Map<Integer, List<GroupDTO>> groupMapCache = new HashMap<Integer, List<GroupDTO>>();

	private static Map<Integer, Map<Integer, TeamDTO>> teamMapCache = new HashMap<Integer, Map<Integer, TeamDTO>>();

	private static final int LEAGUE_FUSSBALL = 1;
	private static final int LEAGUE_TEST = 44;

	private SportsdataSoap getService() {
		if (service == null) {
			service = new Sportsdata().getPort(SportsdataSoap.class);
		}
		return service;
	}

	public void getSports() {
		log.info("getSports()");
		ArrayOfSport sportArray = getService().getAvailSports();
		for (Sport l : sportArray.getSport()) {
			log.info("l=" + l.getSportsID() + ";" + l.getSportsName());
		}
	}

	public List<LeagueDTO> getLeagues() {
		log.info("getLeagues()");
		if (leagueCache == null) {
			leagueCache = new ArrayList<LeagueDTO>();
			leagueMapCache = new HashMap<Integer, LeagueDTO>();

			// List<Sport> sports = getService().getAvailSports().getSport();
			// for (Sport s : sports) {
			// System.out.println("s=" + s.getSportsID() + ";"
			// + s.getSportsName());
			// }

			// loop over all leagues
			ArrayOfLeague fussballArray = getService()
					.getAvailLeaguesBySports(LEAGUE_FUSSBALL);
			for (League l : fussballArray.getLeague()) {

				LeagueDTO leagueDTO = new LeagueDTO();
				leagueDTO.leagueId = l.getLeagueID();
				leagueDTO.saison = l.getLeagueSaison();
				leagueDTO.shortName = l.getLeagueShortcut();
				leagueDTO.title = l.getLeagueName();

				// skip years from 2000-2015
				if (leagueDTO.saison.matches("200[0-9]|201[0-5]")) {
					continue;
				}

				leagueCache.add(leagueDTO);
				leagueMapCache.put(l.getLeagueID(), leagueDTO);
			}

			Collections.sort(leagueCache, new Comparator<LeagueDTO>() {

				@Override
				public int compare(LeagueDTO o1, LeagueDTO o2) {
					int r = Integer.parseInt(o1.saison)
							- Integer.parseInt(o2.saison);
					if (r != 0) {
						return r;
					}

					r = o1.title.compareTo(o2.title);
					if (r != 0) {
						return r;
					}

					return o1.leagueId - o2.leagueId;
				}
			});
		}

		return leagueCache;
	}

	public List<GroupDTO> getGroups(int leagueId) {

		log.info("getGroups(" + leagueId + ")");
		List<GroupDTO> allGroups = groupMapCache.get(leagueId);
		if (allGroups == null) {
			allGroups = new ArrayList<GroupDTO>();
			groupMapCache.put(leagueId, allGroups);

			getLeagues();
			LeagueDTO league = leagueMapCache.get(leagueId);

			log.info("league=" + league);
			if (league == null) {
				log.warn("leage " + leagueId + " not found in "
						+ leagueMapCache.keySet());
			}

			//
			// group
			//
			log.info("Group");
			ArrayOfGroup groupArray = getService()
					.getAvailGroups(league.shortName, league.saison);
			List<Group> groups = groupArray.getGroup();
			for (Group g : groups) {
				log.info("  Group: " + g.getGroupID() + "," + g.getGroupName()
						+ ";" + g.getGroupOrderID());
				GroupDTO group = new GroupDTO();
				group.groupId = g.getGroupID();
				group.groupOrderId = g.getGroupOrderID();
				group.leagueId = league.leagueId;
				group.season = league.saison;
				group.name = g.getGroupName();

				// determine last, first date
				long lastMatchDate = 0;
				long firstMatchDate = Long.MAX_VALUE;

				//
				// all matches per group
				//
				List<MatchDTO> allMatches = getMatches(group.leagueId,
						group.groupId);

				for (MatchDTO m : allMatches) {
					if (lastMatchDate < m.matchdate.getTime()) {
						lastMatchDate = m.matchdate.getTime();
					}
					if (firstMatchDate > m.matchdate.getTime()) {
						firstMatchDate = m.matchdate.getTime();
					}
				}

				group.firstMatchdate = new Date(firstMatchDate);
				group.lastMatchdate = new Date(lastMatchDate);

				allGroups.add(group);
			}

			Collections.sort(allGroups, new Comparator<GroupDTO>() {

				@Override
				public int compare(GroupDTO o1, GroupDTO o2) {
					int r = o1.groupOrderId - o2.groupOrderId;
					if (r != 0) {
						return r;
					}

					return o1.groupId - o2.groupId;
				}
			});
		}

		return allGroups;
	}

	public List<TeamDTO> getTeams(int leagueId) {
		List<TeamDTO> allTeams = new ArrayList<TeamDTO>();

		getLeagues();
		LeagueDTO league = leagueMapCache.get(leagueId);

		ArrayOfTeam teamArray = getService()
				.getTeamsByLeagueSaison(league.shortName, league.saison);
		for (Team t : teamArray.getTeam()) {
			log.info("  Team: " + t.getTeamID() + "," + t.getTeamName() + ", "
					+ t.getTeamIconURL());

			TeamDTO team = new TeamDTO();
			team.teamId = t.getTeamID();
			team.name = t.getTeamName();
			team.url = t.getTeamIconURL();
			allTeams.add(team);
		}

		return allTeams;
	}

	private Map<Integer, TeamDTO> getTeamMap(int leagueId) {

		getLeagues();
		LeagueDTO league = leagueMapCache.get(leagueId);

		log.info("league=" + league);

		Map<Integer, TeamDTO> teamMap = teamMapCache.get(leagueId);
		if (teamMap == null) {
			teamMap = new LinkedHashMap<Integer, TeamDTO>();
			teamMapCache.put(leagueId, teamMap);

			//
			// team
			//
			log.info("Team");
			ArrayOfTeam teamArray = getService()
					.getTeamsByLeagueSaison(league.shortName, league.saison);
			for (Team t : teamArray.getTeam()) {
				log.info("  Team: " + t.getTeamID() + "," + t.getTeamName()
						+ ", " + t.getTeamIconURL());

				TeamDTO team = new TeamDTO();
				team.teamId = t.getTeamID();
				team.name = t.getTeamName();
				teamMap.put(team.teamId, team);
			}
		}

		return teamMap;
	}

	public List<MatchDTO> getMatches(int leagueId, int groupId) {

		log.info("getMatches(" + leagueId + ", " + groupId + ")");

		List<MatchDTO> allMatches = matchMapCache.get(leagueId);
		if (allMatches == null) {
			allMatches = new ArrayList<MatchDTO>();
			matchMapCache.put(leagueId, allMatches);

			getLeagues();
			LeagueDTO league = leagueMapCache.get(leagueId);
			log.info("league=" + league);

			//
			// match
			//
			log.info("Match");
			ArrayOfMatchdata matchDataArray = getService()
					.getMatchdataByLeagueSaison(league.shortName,
							league.saison);
			List<Matchdata> matchDatas = matchDataArray.getMatchdata();
			for (Matchdata m : matchDatas) {

				MatchDTO match = new MatchDTO();
				match.matchId = m.getMatchID();
				match.leagueId = m.getLeagueID();
				match.season = m.getLeagueSaison();
				match.groupId = m.getGroupID();
				match.groupOrderId = m.getGroupOrderID();
				match.team1 = new TeamDTO();
				match.team1.name = getTeamMap(m.getLeagueID())
						.get(m.getIdTeam1()).name;
				match.team1.teamId = m.getIdTeam1();
				match.team2 = new TeamDTO();
				match.team2.name = getTeamMap(m.getLeagueID())
						.get(m.getIdTeam2()).name;
				match.team2.teamId = m.getIdTeam2();
				match.matchdate = new Date(m.getMatchDateTimeUTC()
						.toGregorianCalendar().getTimeInMillis());

				match.lastUpdate = new Date(m.getLastUpdate()
						.toGregorianCalendar().getTimeInMillis());

				match.finished = m.isMatchIsFinished();
				if (m.isMatchIsFinished()) {

					List<MatchResult> result = m.getMatchResults()
							.getMatchResult();
					log.info("result=" + result);
					MatchResult res = getLastMatchresult(result);
					if (res != null) {
						match.score1 = res.getPointsTeam1();
						match.score2 = res.getPointsTeam2();
					}
				}

				allMatches.add(match);
			}

			Collections.sort(allMatches, ComparatorUtil.MATCH_COMPARATOR);
		} else {
			log.info("reading from cache");
		}

		List<MatchDTO> ms = new ArrayList<MatchDTO>();
		for (MatchDTO m : allMatches) {
			if (m.groupId == groupId || groupId < 0) {
				ms.add(m);
			}
		}

		return ms;
	}

	public MatchDTO getMatchresult(int matchId) {

		log.info("getMatchResult(" + matchId + ")");

		Matchdata data = getService().getMatchByMatchID(matchId);
		if (data == null) {
			return null;
		}
		if (!data.isMatchIsFinished()) {
			log.info("match is not finished");
			return null;
		}

		List<MatchResult> result = data.getMatchResults().getMatchResult();
		log.info("result=" + result);
		MatchDTO res = null;

		MatchResult r = getLastMatchresult(result);
		if (r == null) {
			log.warn("could not read a result for " + matchId + "; match="
					+ data.getNameTeam1() + ":" + data.getNameTeam2()
					+ "(finished=" + data.isMatchIsFinished() + ")");
			float pastTime = (System.currentTimeMillis()
					- data.getMatchDateTimeUTC().toGregorianCalendar()
							.getTimeInMillis())
					/ 1000 / 60.0f;

			if (!data.isMatchIsFinished() && pastTime > 1.0f) {
				log.warn("past time older than an hour " + pastTime);
			}

		} else {
			res = new MatchDTO();
			res.matchId = matchId;
			res.groupId = data.getGroupID();
			res.groupOrderId = data.getGroupOrderID();
			res.score1 = r.getPointsTeam1();
			res.score2 = r.getPointsTeam2();
		}

		return res;
	}

	private MatchResult getLastMatchresult(List<MatchResult> result) {
		for (MatchResult m : result) {
			log.info("m=" + m.getResultName() + ";" + m.getPointsTeam1() + ";"
					+ m.getPointsTeam2() + ";" + m.getResultTypeName() + "("
					+ m.getResultTypeId() + ")" + "; order="
					+ m.getResultOrderID());
			if ("Endergebnis".equals(m.getResultName())) {
				// Halbzeitergebnis
				return m;
			}
		}

		return null;
	}

}
