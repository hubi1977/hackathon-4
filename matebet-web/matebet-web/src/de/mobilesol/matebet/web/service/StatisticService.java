package de.mobilesol.matebet.web.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.StatisticDAO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.StatisticDTO;

public class StatisticService {
	private static final Logger log = Logger
			.getLogger(StatisticService.class.getName());

	private StatisticDAO dao = new StatisticDAO();
	private ClosedBetService closedBetService = new ClosedBetService();

	public StatisticDTO getStatisctic(int userId) {
		log.info("getStatistic(" + userId + ")");

		StatisticDTO stats = dao.getStatistic(userId);
		stats.waitingBets = new ArrayList<BetDTO>();
		stats.lostBets = new ArrayList<BetDTO>();
		stats.wonBets = new ArrayList<BetDTO>();
		stats.remisBets = new ArrayList<BetDTO>();

		List<BetDTO> bets = closedBetService.getClosedBets(userId);
		for (BetDTO b : bets) {
			if (b.status == BetStatus.DECLINED) {
				continue;
			}

			if (b.resultByUser == null || b.resultByUser.get(userId) == null) {
				stats.waitingBets.add(b);
			} else if (b.resultByUser.get(userId) < 0) {
				stats.lostBets.add(b);
			} else if (b.resultByUser.get(userId) > 0) {
				stats.wonBets.add(b);
			} else {
				stats.remisBets.add(b);
			}
		}

		log.info("stats1=" + stats);
		int l = stats.lostBets.size();
		int w = stats.wonBets.size();
		int r = stats.remisBets.size();

		// workaround
		for (int i = 0; i < stats.lostBetsCount; i++) {
			stats.lostBets.add(null);
		}
		for (int i = 0; i < stats.wonBetsCount; i++) {
			stats.wonBets.add(null);
		}
		for (int i = 0; i < stats.remisBetsCount; i++) {
			stats.remisBets.add(null);
		}

		stats.lostBetsCount += l;
		stats.wonBetsCount += w;
		stats.remisBetsCount += r;
		log.info("stats2=" + stats);

		return stats;
	}
}
