package de.mobilesol.matebet.web.service.facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.service.facebook.dto.FacebookFriendDTO;
import de.mobilesol.matebet.web.service.facebook.dto.FacebookFriendDTO.Friend;
import de.mobilesol.matebet.web.service.facebook.dto.FacebookUserDTO;
import de.mobilesol.matebet.ws.dto.UserProfileDTO;

public class FacebookService {
	private static final Logger log = Logger
			.getLogger(FacebookService.class.getName());

	private UserService userService = new UserService();

	public UserProfileDTO readProfile(String accessToken) {
		URL url;
		try {
			// https://developers.facebook.com/docs/facebook-login/permissions
			url = new URL("https://graph.facebook.com/v2.8/me?access_token="
					+ accessToken
					+ "&fields=id,name,first_name,last_name,age_range,picture,email&format=json");
			InputStream in = url.openStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			try {
				String line;
				StringBuffer strb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					strb.append(line + "\n");
				}

				log.info("response from facebook=" + strb);

				FacebookUserDTO ufb = new Gson().fromJson(strb.toString(),
						FacebookUserDTO.class);

				UserProfileDTO profile = new UserProfileDTO();
				profile.id = "fb:" + ufb.id;
				profile.email = ufb.email;
				profile.name = ufb.name;
				profile.picture = (ufb.picture != null
						&& ufb.picture.data != null
								? ufb.picture.data.get("url")
								: null);
				if (ufb.age_range != null && ufb.age_range.min != null
						&& ufb.age_range.min >= 18) {
					profile.age = "c";
				} else if (ufb.age_range != null && ufb.age_range.min != null
						&& ufb.age_range.min >= 16) {
					profile.age = "b";
				} else if (ufb.age_range == null || ufb.age_range.min == null) {
					profile.age = "0";
				} else {
					profile.age = "a";
				}

				return profile;
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			log.warn("readProfile failed", e);
			e.printStackTrace();
			return null;
		}
	}

	public List<UserDTO> getFriends(String accessToken) {
		URL url;
		List<UserDTO> userFriends = new ArrayList<UserDTO>();

		String furl = "https://graph.facebook.com/v2.8/me/friends?access_token="
				+ accessToken + "&limit=100";
		while (furl != null) {
			try {
				url = new URL(furl);
				furl = null;
				InputStream in = url.openStream();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in, "UTF-8"));
				try {
					String line;
					StringBuffer strb = new StringBuffer();
					while ((line = reader.readLine()) != null) {
						strb.append(line + "\n");
					}

					log.info("limited response friends from facebook=" + strb);
					Gson gson = new Gson();
					FacebookFriendDTO user = gson.fromJson(strb.toString(),
							FacebookFriendDTO.class);

					log.info("found facebook friends #=" + user.data.size());
					for (Friend ff : user.data) {
						UserDTO uf = userService.getUserByExtId("fb:" + ff.id);
						if (uf != null) {
							userFriends.add(uf);
						}
					}

					if (user.paging.next != null
							&& user.paging.next.length() > 0) {
						log.info("another page: " + user.paging.next);
						furl = user.paging.next;
					}

				} finally {
					reader.close();
				}
			} catch (Exception e) {
				log.error("failed", e);
				furl = null;
			}
		}

		log.info("found facebook friends that are also matebet users: "
				+ userFriends);

		return userFriends;
	}
}
