package de.mobilesol.matebet.web.service.master;

import java.util.List;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.LeagueDAO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.util.Util;

public class LeagueService {
	private static final Logger log = Logger
			.getLogger(LeagueService.class.getName());
	private LeagueDAO dao = new LeagueDAO();

	public LeagueDTO addLeague(LeagueDTO a) {
		log.info("addLeague" + a);

		Util.getCache().remove("mb:league:true");
		Util.getCache().remove("mb:league:false");

		return dao.addLeague(a);
	}

	public List<LeagueDTO> getAllLeagues(boolean incDisabled) {
		log.info("getAllLeagues(" + incDisabled + ")");

		final String key = "mb:league:" + incDisabled;
		Cache c = null;
		List<LeagueDTO> l;
		try {
			c = Util.getCache();
			l = (List<LeagueDTO>) c.get(key);
			if (l == null) {
				log.info("reading from db");
				l = dao.getAllLeagues(incDisabled);
				c.put(key, l);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			l = dao.getAllLeagues(incDisabled);
		}

		return l;
	}

	public LeagueDTO getLeague(int id) {
		log.info("getLeague(" + id + ")");

		final String key = "mb:league:" + id;
		Cache c = null;
		LeagueDTO l;
		try {
			c = Util.getCache();
			l = (LeagueDTO) c.get(key);
			if (l == null) {
				log.info("reading from db");
				l = dao.getLeague(id);
				c.put(key, l);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			l = dao.getLeague(id);
		}

		log.info("l=" + l);

		return l;
	}

	public LeagueDTO disableLeague(int id, boolean disable) {
		log.info("disableLeague(" + id + ", " + disable + ")");

		Util.getCache().remove("mb:league:" + id);
		Util.getCache().remove("mb:league:true");
		Util.getCache().remove("mb:league:false");

		return dao.disableLeague(id, disable);
	}

	public LeagueDTO deleteLeague(int id) {
		log.info("deleteLeague(" + id + ")");

		Util.getCache().remove("mb:league:" + id);
		Util.getCache().remove("mb:league:true");
		Util.getCache().remove("mb:league:false");

		// delete cascade
		new BetService().deleteBetsByLeague(id);
		// new GroupDAO().deleteGroupsByLeague(id);
		// new MatchDAO().deleteMatchsByLeague(id);

		LeagueDTO l = dao.deleteLeague(id);
		log.info("l=" + l);

		return l;
	}
}
