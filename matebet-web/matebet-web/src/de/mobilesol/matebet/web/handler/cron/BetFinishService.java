package de.mobilesol.matebet.web.handler.cron;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.apphosting.api.ApiProxy;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.service.PushService;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.service.master.BetService;
import de.mobilesol.matebet.web.service.master.MatchService;
import de.mobilesol.matebet.web.util.CalculatePointsUtil;

public class BetFinishService {

	private static final Logger log = Logger
			.getLogger(BetFinishService.class.getName());

	private BetService betService = new BetService();
	private PushService pushService = new PushService();
	private UserService userService = new UserService();
	private MatchService matchService = new MatchService();

	public List<BetDTO> finishBets() {

		List<BetDTO> closedBets = betService.getBetsByStatus(BetStatus.CLOSED);
		log.info("found " + closedBets.size() + " closed bets");

		List<BetDTO> betsToUpdate = new ArrayList<BetDTO>();

		ApiProxy.flushLogs();
		for (BetDTO bet : closedBets) {
			try {
				log.info("handle bet " + bet.betId);

				MatchTipDTO userMatchTips = bet.matchTips;
				log.info("userMatchTips=" + userMatchTips);

				// determine resultTips
				log.debug("groups=" + bet.groups);
				TipsByMatchByGroupMap resultTips = getResultTips(bet.leagueId,
						bet.groups);
				log.debug("resultTips=" + resultTips);
				ApiProxy.flushLogs();

				CalculatePointsUtil calc = new CalculatePointsUtil();
				bet.resultByMatchByUser = calc.calculatePoints(bet,
						userMatchTips, resultTips);
				ApiProxy.flushLogs();

				if (!calc.isFailed()) {
					log.info("result found, so set results");
					bet.results = calc.getResults(bet.resultByMatchByUser);
					log.info("#results=" + bet.results.size());
					bet.resultByUser = calc.getResultByUser(bet.userIds,
							bet.results);
					log.info("#resultsByUser=" + bet.resultByUser.size());
					bet.resultTips = resultTips;
					log.debug("#resultTips=" + bet.resultTips);

					betsToUpdate.add(bet);
				} else {
					log.info("result not found. Skip bet " + bet.betId + ";");
				}
			} catch (Exception e) {
				log.warn("exception in betFinish " + bet, e);
			}
		}

		log.info("setting bets to finished: " + betsToUpdate.size());

		report(betsToUpdate);

		return betsToUpdate;
	}

	private TipsByMatchByGroupMap getResultTips(int leagueId,
			List<BetGroupDTO> groups) {

		Set<Integer> allMatches = new HashSet<Integer>();
		for (BetGroupDTO g : groups) {
			for (BetMatchDTO m : g.matches) {
				allMatches.add(m.matchId);
			}
		}

		List<MatchDTO> allMatchesInLeague = matchService
				.getMatchesByLeague(leagueId);

		TipsByMatchByGroupMap resultTips = new TipsByMatchByGroupMap();
		resultTips.groups = new HashMap<Integer, MatchTipDTO.TipsByMatchMap>();

		for (MatchDTO m : allMatchesInLeague) {
			if (!allMatches.contains(m.matchId)) {
				continue;
			}

			if (m.score1 != null && m.score2 != null) {
				TipsByMatchMap tbmm = resultTips.groups.get(m.groupId);
				if (tbmm == null) {
					tbmm = new TipsByMatchMap();
					resultTips.groups.put(m.groupId, tbmm);
				}
				if (tbmm.tips == null) {
					tbmm.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
				}
				TipDTO tip = new TipDTO();
				tip.tipTeam1 = m.score1;
				tip.tipTeam2 = m.score2;
				tbmm.tips.put(m.matchId, tip);
			}
		}

		return resultTips;
	}

	private void report(List<BetDTO> betsToUpdate) {

		log.info("reporting bets #=" + betsToUpdate.size());
		// create datastructure for users + opponents
		Map<Integer, List<BetDTO>> reportUserMap = new HashMap<Integer, List<BetDTO>>();
		for (BetDTO b : betsToUpdate) {

			try {
				for (int userId : b.userIds) {
					List<BetDTO> bets = reportUserMap.get(userId);
					if (bets == null) {
						bets = new ArrayList<BetDTO>();
						reportUserMap.put(userId, bets);
					}
					bets.add(b);
				}
			} catch (Exception e) {
				log.warn("error in reporting " + b, e);
			}
		}

		for (Map.Entry<Integer, List<BetDTO>> userEntry : reportUserMap
				.entrySet()) {

			int userId = userEntry.getKey();
			List<BetDTO> bets = userEntry.getValue();

			UserDTO user = userService.getUser(userId, false);
			if (user == null) {
				// if the user has been removed meanwhile
				log.warn("user " + userId + " has been removed?");
				continue;
			}
			log.info("reporting user " + user.name);

			int res = allSame(userId, bets);
			String title = null;
			String subtitle = null;

			for (BetDTO b : bets) {
				b.groups = null;
				b.leagueId = 0;
				b.matchTips = null;
				b.lastMatchdate = null;
				b.confirmedBy = null;
				b.archivedBy = null;
				b.resultByMatchByUser = null;
				b.resultTips = null;
				b.created = null;
				b.leagueName = null;
				b.results = null;
			}
			Map<String, Object> payload = new HashMap<String, Object>();
			payload.put("bets", bets);

			if (bets.size() > 1) {
				switch (res) {
					case -1 :
						title = "Du hast verloren!";
						subtitle = user.name + ", " + bets.size()
								+ " Spiele wurden beendet.";

						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.BET_STATUS, payload));

						break;

					case 1 :
						title = "Du hast gewonnen!";
						subtitle = user.name + ", " + bets.size()
								+ " Spiele wurden beendet.";

						payload.put("betId", "" + bets.get(0).betId);
						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.BET_STATUS, payload));

						break;

					case 0 :
						title = "Remis!";
						subtitle = user.name + ", " + bets.size()
								+ " Spiele wurden beendet.";

						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.CLOSED_BETS, payload));

						break;

					default :
						title = "Spiele beendet.";
						subtitle = user.name + ", " + bets.size()
								+ " Spiele wurden beendet.";

						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.CLOSED_BETS, payload));

						break;
				}

			} else {

				switch (res) {
					case -1 :
						title = "Du hast verloren!";
						subtitle = "Wette '" + bets.get(0).title
								+ "' wurde beendet.";

						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.BET_STATUS, payload));

						break;

					case 1 :
						title = "Du hast gewonnen!";
						subtitle = "Wette '" + bets.get(0).title
								+ "' wurde beendet.";

						payload.put("betId", "" + bets.get(0).betId);
						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.BET_STATUS, payload));

						break;

					case 0 :
						title = "Remis!";
						subtitle = "Wette '" + bets.get(0).title
								+ "' wurde beendet.";

						pushService.send(userId, new PushNotificationDTO(title,
								subtitle, Page.CLOSED_BETS, payload));

						break;

					default :
						log.warn("this must not happen");
						break;
				}

			}

			log.info("send " + title + "; " + subtitle);

		}
	}

	int allSame(int userId, List<BetDTO> bets) {
		log.info("allSame(" + userId + ", " + bets);
		boolean allWon = true;
		boolean allLost = true;
		boolean allRemis = true;

		for (BetDTO bet : bets) {
			if (bet.resultByUser.get(userId) <= 0) {
				allWon = false;
			}
			if (bet.resultByUser.get(userId) >= 0) {
				allLost = false;
			}
			if (bet.resultByUser.get(userId) != 0) {
				allRemis = false;
			}
		}

		if (allWon) {
			return 1;
		}
		if (allLost) {
			return -1;
		}
		if (allRemis) {
			return 0;
		}
		return 99;
	}
}
