package de.mobilesol.matebet.web.handler.cron;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.service.MatchTipService;
import de.mobilesol.matebet.web.service.PushService;
import de.mobilesol.matebet.web.service.master.BetService;
import de.mobilesol.matebet.web.service.master.MatchService;
import de.mobilesol.matebet.web.service.master.TeamService;

public class UserReminderHandler extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(UserReminderHandler.class.getName());

	private Gson gson = new GsonBuilder().setPrettyPrinting()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").create();

	private MatchService matchService = new MatchService();
	private MatchTipService matchTipService = new MatchTipService();
	private TeamService teamService = new TeamService();
	private BetService betService = new BetService();
	private PushService pushService = new PushService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.info("going to remind users");

		List<MatchDTO> matches = matchService.getMatchesBeginningSoon();
		Map<Integer, List<MatchDTO>> matchesByGroup = new HashMap<Integer, List<MatchDTO>>();
		Set<Integer> leagues = new HashSet<Integer>();
		for (MatchDTO m : matches) {
			List<MatchDTO> mm = matchesByGroup.get(m.groupId);
			if (mm == null) {
				mm = new ArrayList<MatchDTO>();
				matchesByGroup.put(m.groupId, mm);
			}
			mm.add(m);
			leagues.add(m.leagueId);
		}

		Map<Integer, Set<MatchDTO>> usersToInfom = new HashMap<Integer, Set<MatchDTO>>();

		// loop leagues
		for (int leagueId : leagues) {
			List<BetDTO> bets = betService.getBetsByLeague(leagueId);
			log.info("bets by league " + leagueId);
			for (BetDTO b : bets) {
				log.info("bet " + b.betId);

				MatchTipDTO userMatchTips = null;

				for (BetGroupDTO group : b.groups) {
					List<MatchDTO> matchesBeginningSoon = matchesByGroup
							.get(group.groupId);

					if (matchesBeginningSoon != null) {
						for (MatchDTO matchSoon : matchesBeginningSoon) {
							// bet covers groups that maybe relevant
							for (BetMatchDTO m : group.matches) {
								if (matchSoon.matchId == m.matchId) {
									// check if matchSoon has not been tipped
									for (Integer userId : b.userIds) {
										Set<MatchDTO> matchSet = usersToInfom
												.get(userId);
										if (matchSet == null) {
											usersToInfom.put(userId,
													new HashSet<MatchDTO>());
										}
									}

									log.info("next match in bet " + b.betId
											+ ";" + matchSoon);

									if (userMatchTips == null) {
										userMatchTips = matchTipService
												.getMatchTips(b.betId, -1);
									}

									for (int userId : b.userIds) {
										boolean userSet = (userMatchTips != null
												&& userMatchTips.groupsByUser
														.get(userId) != null
												&& userMatchTips.groupsByUser
														.get(userId).groups
																.get(group.groupId) != null
												&& userMatchTips.groupsByUser
														.get(userId).groups
																.get(group.groupId).tips
																		.get(m.matchId) != null);

										if (!userSet) {
											usersToInfom.get(userId)
													.add(matchSoon);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// now report
		for (Integer userId : usersToInfom.keySet()) {
			Set<MatchDTO> matchesInfo = usersToInfom.get(userId);
			if (!matchesInfo.isEmpty()) {

				if (matchesInfo.size() == 1) {
					MatchDTO m = matchesInfo.iterator().next();
					TeamDTO t1 = teamService.getTeam(m.team1.teamId);
					TeamDTO t2 = teamService.getTeam(m.team2.teamId);

					pushService.send(userId, new PushNotificationDTO(
							t1.name + " gegen " + t2.name,
							"Das Spiel " + t1.name + " gegen " + t2.name
									+ " beginnt in ca. "
									+ calcTime(m.matchdate.getTime()) + "min.",
							Page.OPEN_BETS, null));

				} else {
					pushService.send(userId, new PushNotificationDTO(
							"Einige Spiele beginnen in Kürze.",
							"Du musst noch Tipps für " + matchesInfo.size()
									+ " Spiele abgeben.",
							Page.OPEN_BETS, null));
				}
			}
		}
		resp.getWriter().write(gson.toJson(usersToInfom));
	}

	int calcTime(long matchTime) {
		int diffInS = (int) ((matchTime - System.currentTimeMillis()) / 1000);
		float ret = diffInS / 600.f;

		return Math.round(ret) * 10;
	}
}
