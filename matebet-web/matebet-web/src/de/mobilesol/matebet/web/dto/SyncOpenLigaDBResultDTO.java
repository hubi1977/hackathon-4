package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SyncOpenLigaDBResultDTO implements Serializable {

	private static final long serialVersionUID = -2763200616391094982L;

	public Boolean severeInconsistency;
	public int groupDeltaCount = 0;
	public int matchDeltaCount = 0;
	public LeagueDTO[] leagueDelta;
	public List<GroupDTO[]> groupDelta = new ArrayList<GroupDTO[]>();
	public List<MatchDTO[]> matchDelta = new ArrayList<MatchDTO[]>();

	@Override
	public String toString() {
		return "SyncOpenLigaDBResultDTO [groupDeltaCount=" + groupDeltaCount
				+ ", matchDeltaCount=" + matchDeltaCount + ", leagueDelta="
				+ Arrays.toString(leagueDelta) + ", groupDelta=" + groupDelta
				+ ", matchDelta=" + matchDelta + "]";
	}

}
