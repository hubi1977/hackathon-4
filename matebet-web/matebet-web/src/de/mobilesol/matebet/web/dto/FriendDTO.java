package de.mobilesol.matebet.web.dto;

import java.io.Serializable;

public class FriendDTO implements Serializable {

	private static final long serialVersionUID = 1659132696694112205L;

	public int friendId;
	public String name;

	public String email, picture;
	public boolean request, alreadyRequested;
	public boolean isFriend;

	@Override
	public String toString() {
		return "FriendDTO [id=" + friendId + ", name=" + name + "]";
	}

}
