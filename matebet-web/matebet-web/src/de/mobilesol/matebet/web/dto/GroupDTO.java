package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;

public class GroupDTO implements Serializable {

	private static final long serialVersionUID = 2617399877233170229L;

	public int groupId, leagueId, groupOrderId;
	public String season;
	public String name;

	public Date firstMatchdate;
	public Date lastMatchdate;

	@Override
	public String toString() {
		return "GroupDTO [groupId=" + groupId + ", leagueId=" + leagueId
				+ ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + groupId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupDTO other = (GroupDTO) obj;
		if (groupId != other.groupId)
			return false;
		return true;
	}
}
