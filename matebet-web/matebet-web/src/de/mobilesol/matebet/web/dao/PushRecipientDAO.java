package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO.DeviceDTO;

public class PushRecipientDAO {
	private static final Logger log = Logger
			.getLogger(PushRecipientDAO.class.getName());
	private static final String TABLE = "push_t";

	public void addPush(PushRecipientDTO push) {
		log.info("addPush(" + push.userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, push.userId + "/"
				+ (push.device != null ? push.device.uuid : null));
		Entity e = new Entity(key);
		e.setProperty("token", push.token);
		e.setProperty("userId", push.userId);
		e.setProperty("device", new Gson().toJson(push.device));

		datastore.put(e);
	}

	public void removePush(int userId, String uuid) {
		log.info("removePush(" + userId + "," + uuid + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "/" + uuid);
		datastore.delete(key);
	}

	public void removePushByToken(int userId, String token) {
		log.info("removePushByToken(" + userId + "," + token + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));

		PreparedQuery pq = datastore.prepare(q);
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			PushRecipientDTO p = toObject(e);
			if (p.token.equals(token)) {
				datastore.delete(e.getKey());
			}
		}
	}

	public List<PushRecipientDTO> getPush(int userId) {
		log.info("getPush(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));

		PreparedQuery pq = datastore.prepare(q);

		List<PushRecipientDTO> list = new ArrayList<PushRecipientDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			PushRecipientDTO p = toObject(e);
			list.add(p);
		}
		return list;
	}

	public List<PushRecipientDTO> getAllPush() {
		log.info("getAllPush()");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		PreparedQuery pq = datastore.prepare(q);

		List<PushRecipientDTO> list = new ArrayList<PushRecipientDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			PushRecipientDTO p = toObject(e);
			list.add(p);
		}
		return list;
	}

	private PushRecipientDTO toObject(Entity e) {
		PushRecipientDTO p = new PushRecipientDTO();
		p.token = (String) e.getProperty("token");
		p.userId = ((Number) e.getProperty("userId")).intValue();

		String device = (String) e.getProperty("device");
		if (device != null) {
			p.device = new Gson().fromJson(device, DeviceDTO.class);
		}

		return p;
	}
}
