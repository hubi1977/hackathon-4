package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.UserDTO;

public class UserDAO {
	private static final Logger log = Logger.getLogger(UserDAO.class.getName());
	private static final String TABLE = "user_t";
	private ShardedCounter shard;

	public UserDAO() {
		shard = new ShardedCounter("user_s", 10000000);
	}

	public UserDTO addUser(UserDTO a) {
		log.info("addUser" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		a.userId = shard.getCount();
		shard.increment();
		Key key = KeyFactory.createKey(TABLE, a.userId);
		try {
			datastore.get(key);
			throw new IllegalStateException(
					"id " + a.userId + " already exists,");
		} catch (EntityNotFoundException e1) {
			a.created = new Date();
			Entity e = toEntity(new Entity(key), a);
			datastore.put(e);

			return a;
		}
	}

	public UserDTO updateUser(UserDTO a) throws EntityNotFoundException {
		log.info("updateUser" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		UserDTO oldUser = getUser(a.userId, true);
		if (a.name == null || a.name.length() == 0) {
			a.name = oldUser.name;
		}
		if (a.age == null || a.age.length() == 0) {
			a.age = oldUser.age;
		}
		if (a.extId == null || a.extId.length() == 0) {
			a.extId = oldUser.extId;
		}
		a.created = oldUser.created;
		if (a.email == null || a.email.length() == 0) {
			a.email = oldUser.email;
		}
		if (a.password == null || a.password.length() == 0) {
			a.password = oldUser.password;
		}
		if (a.picture == null || a.picture.length() == 0) {
			a.picture = oldUser.picture;
		}

		Key key = KeyFactory.createKey(TABLE, a.userId);
		Entity e = toEntity(datastore.get(key), a);
		e.setProperty("modified", new Date());
		datastore.put(e);

		a = toObject(e, false);

		return a;
	}

	public UserDTO getUser(int id, boolean incPw) {

		UserDTO user = new UserDTO();
		user.userId = 1;
		user.name = "Beers forever";
		
		return user;
	}

	public void deleteUser(int id) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, id);
		try {
			Entity e = datastore.get(key);
			datastore.delete(e.getKey());
		} catch (EntityNotFoundException ex) {
			log.warn("entiy with key " + key + " does not exist.");
		}
	}

	public List<UserDTO> getAllUsers(String query) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		List<UserDTO> users = new ArrayList<UserDTO>();
		Query q = new Query(TABLE);
		if (query != null) {
			ArrayList<Filter> a = new ArrayList<Filter>();
			a.add(new FilterPredicate("namel",
					FilterOperator.GREATER_THAN_OR_EQUAL, query.toLowerCase()));
			a.add(new FilterPredicate("namel", FilterOperator.LESS_THAN,
					query.toLowerCase() + "\uFFFD"));

			Filter f = (new CompositeFilter(CompositeFilterOperator.AND, a));
			q.setFilter(f);
		}

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			UserDTO a = toObject(it.next(), false);
			users.add(a);
		}

		return users;
	}

	public List<UserDTO> getUsers(Set<String> emails) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		List<String> emailList = new ArrayList<String>(emails);
		List<UserDTO> users = new ArrayList<UserDTO>();

		while (!emailList.isEmpty()) {

			log.info("email=" + emailList.size());
			Query q = new Query(TABLE);
			ArrayList<Filter> ar = new ArrayList<Filter>();

			int i = 0;
			if (emailList.size() > 1) {
				for (ListIterator<String> lit = emailList.listIterator(); lit
						.hasNext(); i++) {
					ar.add(new FilterPredicate("email", FilterOperator.EQUAL,
							lit.next().toLowerCase()));
					lit.remove();
					if (i > 20) {
						break;
					}
				}
				q.setFilter(
						new CompositeFilter(CompositeFilterOperator.OR, ar));
			} else {
				q.setFilter(new FilterPredicate("email", FilterOperator.EQUAL,
						emailList.get(0).toLowerCase()));

			}
			PreparedQuery pq = datastore.prepare(q);

			Iterator<Entity> it = pq.asIterator();
			while (it.hasNext()) {
				UserDTO a = toObject(it.next(), false);
				users.add(a);
			}
		}

		return users;
	}

	public UserDTO getUserByEmail(String email, boolean incPw) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("email", FilterOperator.EQUAL,
				email.toLowerCase()));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			UserDTO a = toObject(it.next(), incPw);
			return a;
		} else {
			log.warn("did not find a user " + email);
		}

		return null;
	}

	public UserDTO getUserByExtId(String extId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("extId", FilterOperator.EQUAL, extId));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			UserDTO a = toObject(it.next(), true);
			return a;
		} else {
			log.warn("did not find a user " + extId);
		}

		return null;
	}

	public UserDTO getUserByName(String name, boolean incPw) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("namel", FilterOperator.EQUAL,
				name.toLowerCase()));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			UserDTO a = toObject(it.next(), incPw);
			return a;
		}

		return null;
	}

	private Entity toEntity(Entity e, UserDTO a) {

		BaseUserDTO base = new BaseUserDTO();
		base.age = a.age;
		base.name = a.name;
		base.password = a.password;
		base.picture = a.picture;
		base.userId = a.userId;
		base.created = a.created;

		Gson gson = new Gson();
		e.setProperty("master", gson.toJson(base));

		if (a.email != null) {
			e.setProperty("email", a.email.toLowerCase());
		}
		if (a.name != null) {
			e.setProperty("namel", a.name.toLowerCase());
		}
		if (a.extId != null) {
			e.setProperty("extId", a.extId);
		}

		return e;
	}

	private UserDTO toObject(Entity e, boolean incPw) {
		UserDTO a = new UserDTO();

		Gson gson = new Gson();
		String master = (String) e.getProperty("master");
		BaseUserDTO base = gson.fromJson(master, BaseUserDTO.class);

		a.age = base.age;
		a.name = base.name;
		if (incPw) {
			a.password = base.password;
		}
		a.picture = base.picture;
		if (a.picture == null) {
			a.picture = "http://api.matebet.net/images/photo.jpg";
		}
		a.userId = base.userId;
		// a.created = base.created;

		a.email = (String) e.getProperty("email");
		a.extId = (String) e.getProperty("extId");

		if (base.password == null) {
			a.extAuth = true;
		}

		return a;
	}

	public static class BaseUserDTO {
		public int userId;
		public String name, password, picture;

		public String age;

		public Date created;

	}
}
