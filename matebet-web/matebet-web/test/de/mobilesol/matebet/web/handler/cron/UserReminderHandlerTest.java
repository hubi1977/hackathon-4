package de.mobilesol.matebet.web.handler.cron;

import org.junit.Assert;
import org.junit.Test;

public class UserReminderHandlerTest {

	private UserReminderHandler userReminder = new UserReminderHandler();

	@Test
	public void calcTime() {
		long gameStarts = System.currentTimeMillis() + 16 * 60 * 1000;
		Assert.assertEquals(userReminder.calcTime(gameStarts), 20);

		gameStarts = System.currentTimeMillis() + 14 * 60 * 1000;
		Assert.assertEquals(userReminder.calcTime(gameStarts), 10);

		gameStarts = System.currentTimeMillis() + 56 * 60 * 1000;
		Assert.assertEquals(userReminder.calcTime(gameStarts), 60);
	}
}
