var pControllers = angular.module('leagueControllers', []);

pControllers.controller('LeagueCtrl', ['$scope', '$http',
   function($scope, $http, $routeParams) {

       $scope.progressloading = true;
       $http({
           url: '/rs/league/list?disabled=true',
           method: 'GET'
           }).success(function(data) {
               $scope.progressloading = false;
               $scope.leagues = data;

           }).error(function(data, status, headers, config) {
               $scope.progressloading = false;
           });

        $scope.disableLeague = function(leagueId) {
        	   console.log('disable ' + leagueId);
            if (confirm("Liga " + leagueId + " wirklich deaktivieren?") == true) {
        
                $scope.progressloading = true;
                $http({
                    method: 'POST',
                    url: '/rs/league/disable/' + leagueId
                }).success(
                    function(data, status, headers,
                        config) {
        
                        $scope.progressloading = false;
                        
                        for (var i = 0; i < $scope.leagues.length; i++) {
                            if ($scope.leagues[i].leagueId == leagueId) {
                        	$scope.leagues[i] = data;
                            }
            	    	}
                    }).error(
                    function(data, status, headers,
                        config) {
        
                        $scope.progressloading = false;
                        alert('something wrong here: '
                 	       + JSON.stringify(data)
                 	       + ', ' + status);
                    });
            }
        }


        $scope.enableLeague = function(leagueId) {
 	   console.log('enable ' + leagueId);
 
 
     if (confirm("Liga " + leagueId + " wirklich aktivieren?") == true) {
 
         $scope.progressloading = true;
         $http({
             method: 'POST',
             url: '/rs/league/enable/' + leagueId
         }).success(
             function(data, status, headers,
                 config) {
 
                 $scope.progressloading = false;
                 for (var i = 0; i < $scope.leagues.length; i++) {
                     if ($scope.leagues[i].leagueId == leagueId) {
                 	$scope.leagues[i] = data;
                     }
     	    	}
             }).error(
             function(data, status, headers,
                 config) {
 
                 $scope.progressloading = false;
                 alert('something wrong here: '
          	       + JSON.stringify(data)
          	       + ', ' + status);
             });
     }
 }

}



   ]);

pControllers.controller('LeagueGroupCtrl', ['$scope', '$http', '$routeParams',
   function($scope, $http, $routeParams) {

       $scope.progressloading = true;
       $http({
           url: '/rs/group/list/' + $routeParams.leagueId,
           method: 'GET'
           }).success(function(data) {
               $scope.progressloading = false;
               $scope.leagueId = $routeParams.leagueId;
               $scope.groups = data;

           }).error(function(data, status, headers, config) {
               $scope.progressloading = false;
           });
       
       
       $scope.deleteLeague = function(leagueId) {
	   console.log('delete ' + leagueId);

       
           if (confirm("Liga " + leagueId + " wirklich löschen?") == true) {

               $scope.progressloading = true;
               $http({
                   method: 'POST',
                   url: '/rs/league/delete/' + leagueId
               }).success(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                   }).error(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                       alert('something wrong here: '
                	       + JSON.stringify(data)
                	       + ', ' + status);
                   });
           }
       }

       $scope.syncLeague = function(leagueId) {
	   console.log('sync ' + leagueId);

       
           if (confirm("Liga " + leagueId + " wirklich mit OpenLigaDB synchonisieren?") == true) {

               $scope.progressloading = true;
               $http({
                   method: 'GET',
                   url: '/rs/openligadb/fix/' + leagueId + '?change=true'
               }).success(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                       confirm('Successfully synced: ' + data.groupDeltaCount + ' groups and '
                	       + data.matchDeltaCount + ' matches\n\n' + JSON.stringify(data));
                   }).error(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                       alert('something wrong here: '
                	       + JSON.stringify(data)
                	       + ', ' + status);
                   });
           }
       }

    }
]);

pControllers.controller('LeagueMatchesCtrl', ['$scope', '$http', '$routeParams',
    function($scope, $http, $routeParams) {

        $scope.progressloading = true;
        $http({
            url: '/rs/match/list/' + $routeParams.groupId,
            method: 'GET'
        }).success(function(data) {
            $scope.progressloading = false;
            $scope.matches = data;

        }).error(function(data, status, headers, config) {
            $scope.progressloading = false;
        });
    }
]);

